/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author luism
 */
public class bomba {
    int numbomba;
    float capabomba=200,acumulitros;
    String gasotipo;
    double gasoprecio=20.91;

    
    public void setiniciarbomba(){
        this.numbomba=numbomba;
        this.gasotipo=gasotipo;
        this.gasoprecio=gasoprecio;
        this.capabomba=200;
    }
    public void setinventariobomba(){
        this.capabomba=this.capabomba;
    }
    public void setVendergasolina(){
        this.gasoprecio=gasoprecio;
    }
    public void setVentastotales(){
        this.acumulitros=this.acumulitros;
    }
    
    
    public float getInventariobomba(){
        return this.capabomba - this.acumulitros;
    }
    
    public double getVentastotales(){
        return this.acumulitros * this.gasoprecio;
    }
    
    public double getVendergasolina(float cantidad){
        if (this.capabomba >= cantidad) {
            this.capabomba -= cantidad;
            this.acumulitros += cantidad;
            return cantidad * this.gasoprecio;
        } else {
            System.out.println("No hay suficiente gasolina disponible.");
            return 0;
        }
    }
}
